const path = require('path');
const CracoLessPlugin = require('craco-less');
const pathResolve = pathUrl => path.join(__dirname, pathUrl);

module.exports = {
  devServer: {
    proxy: {
      "/api": { 
        target: "https://test2-3grzj6qgbb48dea3-1302671208.ap-shanghai.app.tcloudbase.com",
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          "^/api": ""
        }
      }
    }
  },
  webpack: {
    alias: {
      '@': pathResolve('src'),
      'features': '@/features',
      'components': '@/components',
      'api': '@/api',
      'assets':'@/assets',
      'context':'@/context',
      'utils':'@/utils',
    }
  },
  plugins: [{
    plugin: CracoLessPlugin,
    options: {
      lessLoaderOptions: {
        lessOptions: {
          javascriptEnabled: true,
        },
      },
    },
  }, ],
}