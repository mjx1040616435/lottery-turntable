// 时间转换
export function changeDate(timestamp) {
  let date = new Date(timestamp);
  let year = date.getFullYear();
  let month = date.getMonth() + 1;
  let day = date.getDate();
  let hour = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
  let minute = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
  return `${year}年${month}月${day}日 ${hour}:${minute}`
}