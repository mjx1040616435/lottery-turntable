import {
  request
} from "./request";

const baseUrl = '/modules/adminPage';
const roulette = 'manageRoulette';
// 获取所有转盘信息总览
export function getViewRoulettes() {
  return request('post', `${baseUrl}/${roulette}/viewRoulettes`);
}

// 新增转盘
export function addRoulette(data) {
  return request('post', `${baseUrl}/${roulette}/newRoulette`, data);
}

// 删除转盘
export function deleteRoulette(roulette_id) {
  return request('post', `${baseUrl}/${roulette}/deleteRoulette`, {
    roulette_id
  });
}

const prize = "managePrize"
// 获取所有奖品
export function getViewPrizes(){
  return request('post', `${baseUrl}/${prize}/viewPrizes`);
}

// 新增奖品
export function addPrize(data) {
  const formData = new FormData();
  formData.append('prize_name', data.prize_name);
  formData.append('prize_picture', data.prize_picture);
  return request('post', `${baseUrl}/${prize}/newPrize`, formData, {'Content-Type': 'multipart/form-data'});
}

// 删除奖品
export function deletePrize(prize_id) {
  return request('post', `${baseUrl}/${prize}/deletePrize`, {
    prize_id
  });
}