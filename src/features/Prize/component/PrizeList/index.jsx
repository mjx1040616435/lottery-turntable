import React, { useEffect, useState } from "react";
import PrizeBox from "components/PrizeBox";
import Loading from "components/Loading";
import { getViewPrizes } from "api/backend";
import "./index.less";

export default function PrizeList() {
  const [prizeList, setPrizeList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  // 获取数据
  const getPrizeData = async () => {
    const prizeData = await getViewPrizes();
    setPrizeList(prizeData);
    setIsLoading(false);
  };
  useEffect(() => {
    getPrizeData();
  }, []);

  return (
    <div className="draggle-list-container">
      <div className="draggle-theme">奖品列表</div>
      {isLoading ? (
        <Loading isLoading={isLoading} />
      ) : (
        <div className="draggle-list">
          {prizeList.map((item) => (
            <PrizeBox key={item._id} prizeInfo={item} />
          ))}
        </div>
      )}
    </div>
  );
}
