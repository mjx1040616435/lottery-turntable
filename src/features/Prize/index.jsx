import React from "react";
import PrizeList from "./component/PrizeList";
import AddPrize from "./component/AddPrize";
import "./index.less";
// 奖品管理
export default function Prize() {
  return (
    <div className="prize-page-container">
      <PrizeList />
      <AddPrize />
    </div>
  );
}
