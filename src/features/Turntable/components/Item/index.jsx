import React, { Component } from 'react'
import './index.css'
export default class Item extends Component {
    render() {
        const { selectedId, ItemId}=this.props
        return (
            <div className={selectedId === ItemId ? 'selected_item' : 'item'}>
                <img src={this.props.ImgUrl} alt="img" className="img"/>
                <p className="prize_name">{this.props.name}</p>
            </div>
        )
    }
}
