import React, { Component } from 'react'
import PubSub from 'pubsub-js'
import List from '../List'
import './index.css'
export default class Show extends Component {
    state= {
        contents:[],
    }
    componentDidMount=()=>{
        PubSub.subscribe('show',(_,obj)=>{
            if(this.state.contents.length===0) {
                this.setState({ contents: [obj] })
            }else {
                this.setState({ contents: [...this.state.contents, obj] })
            }
            console.log(this.state);
        })
    }
    render() {
        const {contents}=this.state
        return (
            <div className="show">
                <h4 className="show_head">所获奖品清单</h4>
                <div className="show_content">
                    {
                        contents.map((prize,index) => {
                            return <List key={index} content={prize}/>
                        })
                    }
                </div>
            </div>
        )
    }
}
