import React, { Component } from 'react'
import PubSub from 'pubsub-js'
import './index.css'
export default class Popover extends Component {
    state={
        appear:false,
        title:'',
        award:'',
        isTrue:false
    }
    disappear=(event)=> {
       this.setState({appear:false,isTrue:false})
    }
    componentDidMount() {
        //只要有人发布pop,函数将会被调用 _是占位符
        this.token = PubSub.subscribe('pop_title', (_, pop_title) => {
            this.setState({title:pop_title,appear:true})
            //console.log(pop_title);
        })
        this.token1 = PubSub.subscribe('pop', (_,stateObj) => {
            this.setState(stateObj)
            //console.log(stateObj);
        })
    }
    //组件将被卸载前
    componentWillUnmount() {
        PubSub.unsubscribe(this.token)
        PubSub.unsubscribe(this.token1)
    }
    render() {
        const {appear,award,title,isTrue}=this.state
        return (
            <div className={appear===true?"pop_box":"pop_box_hide"}>
                <div className="pop_header">
                    友情提示
                </div>
                <div className="pop_content">
                    <h1 className="pop_title">
                        {title}
                    </h1>
                    <p className={isTrue === true ? "pop_desc" : "pop_desc_hide"}>
                        {award}一个，请联系管理员！！
                    </p>
                    <div className="pop_button" onClick={this.disappear}>
                        我知道了
                    </div>
                </div>
            </div>
        )
    }
}
