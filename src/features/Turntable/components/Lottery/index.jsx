import React, { Component } from 'react'
import Item from '../Item'
import Popover from '../Popover'
import './index.css'
import PubSub from 'pubsub-js'
import { getLottery } from 'api/turntable'
import { getViewRoulettes} from 'api/backend'


export default class Lottery extends Component {
    //状态初始化
    state= {
        //九宫格格子标号
        list:[1,2,3,4,5,6,7,8],
        //初始金额
        money:1000,
        //最终被选择的奖品ID
        prize_id:'',
        //选中奖品所在的格子号
        lucky_id:'',
        //转动时被选中的格子ID
        selectedId:'',
        //是否正在抽奖
        isRolling:false,
        //是否中奖
        isPrized:true,
        //轮盘号
        roulette_id:1,
        //转盘上有多少个奖品
        slot_num: '',
        //轮盘对应的奖品信息汇总（按照轮盘格子顺序存放）
        prizes:'' , 
        //奖品图片url汇总 
        picture_urls:'',
        //奖品名字汇总
        prize_names:'',
        //奖品id汇总
        prize_ids:''
    }
    
    componentDidMount=()=> {
        //获取转盘情况
        getViewRoulettes().then(res=> {
            console.log(res);
            this.setState({ roulette_id:res[0].roulette_id, slot_num: res[0].slot_num, prizes: res[0].prizes, prize_ids: res[0].prize_ids })
            let arr_urls = this.state.prizes.map((obj)=> {
                return obj.picture_url
            })
            let arr_names = this.state.prizes.map((obj) => {
                return obj.prize_name
            })
            this.setState({ picture_urls: arr_urls,prize_names:arr_names })
        })
    }
    handleStart=()=> {
        //判断是否正在抽奖
        if(this.state.isRolling) {
            PubSub.publish('pop_title', '正在运转中，请勿重复点击')
            return 
        }
        else if(this.state.money<200) {
            PubSub.publish('pop_title', '矿石数量不足')
            return
        }
        this.setState({
            isRolling:true,
        },()=> {
            this.handlePlay()
        })
    }
    handlePlay= async ()=> {
        console.log('游戏开始了');
        //prize这里还需要接收返回的参数
        getLottery(this.state.roulette_id).then(res=>{
            this.setState({prize_id:res.prize_id},()=>{
                this.startPlay()
                console.log(this.state);
            })
        }).catch(reason=>{
            console.log(reason);
        })
    }
    startPlay=()=>{
        //选中的奖品的格子号
        let count_num = 0
        let last_count = -1
        this.state.prize_ids.map((Obj) => {
            count_num += 1
            if (Obj === this.state.prize_id) {
                last_count = count_num
                return count_num
            }
            return -1
        })
        this.setState({
            //将奖品号转成对应的格子号
            lucky_id: last_count,
            money: this.state.money - 200
        },()=>{
            if (this.state.prize_names[this.state.lucky_id-1] === "bug") {
                this.setState({ isPrized: false })
            }else {
                this.setState({ isPrized: true })
            }
            
        })
        PubSub.publish('money', this.state.money - 200)
        // 抽奖正式开始↓↓
        let count = 0;
        let t = 40;
        let num;
        this.begin = setInterval(this.fn = () => {
            if (this.state.selectedId === this.state.lucky_id && count > 16) {
                clearInterval(this.begin)
                this.setState({
                    isRolling: false
                })
                //这里需要判断是否中奖，向弹窗传入
                
                if(this.state.isPrized) {
                    PubSub.publish('pop', { title: '恭喜你中奖了', award: `${this.state.prize_names[this.state.lucky_id-1]}`, isTrue: true, appear: true })
                    PubSub.publish('show', this.state.prize_names[this.state.lucky_id - 1])
                }
                else {
                    PubSub.publish('pop', { title: '很遗憾你没有中奖', award: `${this.state.prize_names[this.state.lucky_id - 1]}`, isTrue: false, appear: true })
                }
                //PubSub.publish('appear', true)
                return
            }
            // 以下是动画执行时对id的判断
            if (this.state.selectedId === '') {
                num = 0
                this.setState({
                    selectedId: num
                })
            } else {
                num = this.state.selectedId
                if (num === 8) {
                    num = 0
                    this.setState({
                        selectedId: num
                    })
                } else {
                    num = num + 1
                    this.setState({
                        selectedId: num
                    })
                }
            }
            count += 1
            //减缓速度
            t += 10
            clearInterval(this.begin)
            this.begin = setInterval(this.fn, t)
        }, t)
    }
        
    render() {
        const { list,picture_urls, selectedId, prize_names}=this.state
        return (
            <div className="lottery">
                <div className="left-light sider-light">
                    <i></i><i></i><i></i>
                </div>
                <div className="top-light sider-light">
                    <i></i><i></i><i></i><i></i><i></i>
                </div>
                <div className="right-light sider-light">
                    <i></i><i></i><i></i>
                </div>
                <div className="bottom-light sider-light">
                    <i></i><i></i><i></i><i></i><i></i>
                </div>
                <div className="lottery_content">
                    {/* content里面放置奖品名字,selectedId里面放置转盘转动时所在的格子号 */}
                    <Item ItemId={list[0]} selectedId={selectedId} ImgUrl={picture_urls[0]} name={prize_names[0]}/>
                    <Item ItemId={list[1]} selectedId={selectedId} ImgUrl={picture_urls[1]} name={prize_names[1]}/>
                    <Item ItemId={list[2]} selectedId={selectedId} ImgUrl={picture_urls[2]} name={prize_names[2]}/>
                    <Item ItemId={list[7]} selectedId={selectedId} ImgUrl={picture_urls[7]} name={prize_names[7]}/>
                    <div className="btn_start" onClick={this.handleStart}>
                        <h3>开始</h3>
                        <h4>200矿石/次</h4>
                    </div>
                    <Item ItemId={list[3]}  selectedId={selectedId} ImgUrl={picture_urls[3]} name={prize_names[3]}/>
                    <Item ItemId={list[6]}  selectedId={selectedId} ImgUrl={picture_urls[6]} name={prize_names[6]}/>
                    <Item ItemId={list[5]}  selectedId={selectedId} ImgUrl={picture_urls[5]} name={prize_names[5]}/>
                    <Item ItemId={list[4]}  selectedId={selectedId} ImgUrl={picture_urls[4]} name={prize_names[4]}/>
                </div> name={prize_names[0]}
                <Popover />
            </div>
        )
    }
}
