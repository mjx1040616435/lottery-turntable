import React, { Component } from 'react'
import './index.css'
import PubSub from 'pubsub-js'
export default class Header extends Component {
    state={AllMoney:1000}
    //组件一挂载就执行
    componentDidMount() {
        //只要有人发布money,函数将会被调用 _是占位符
        this.token = PubSub.subscribe('money', (_, money) => {
            this.setState({AllMoney:money})
            //const money=stateObj
        })
    }
    //组件将被卸载前
    componentWillUnmount() {
        PubSub.unsubscribe(this.token)
    }
    render() {
        return (
            <div className="Header">
                <h4 >幸运抽奖</h4>
                <h5>当前矿石数：{this.state.AllMoney}</h5>
            </div>
        )
    }
}
