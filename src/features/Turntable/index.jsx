import React, { Component } from "react";
import Header from "./components/Header";
import Lottery from "./components/Lottery";
import Show from "./components/Show";
// import Popover from '../../components/Popover'
import "./index.css";
// 转盘

export default class Turntable extends Component {
  render() {
    return (
      <div className="big_box">
        <div className="box">
          <Header />
          <Lottery />
        </div>
        <Show/>
      </div>
    )
  }
}
