import React, { useEffect, useState } from "react";
import PrizeBox from "components/PrizeBox";
import Loading from "components/Loading";
import { getViewPrizes } from "api/backend";
import "./index.less";

export default function DraggleList() {
  const [prizeList, setPrizeList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    const getData = async () => {
      const prizeData = await getViewPrizes();
      setPrizeList(prizeData);
      setIsLoading(false);
    };
    getData();
  }, []);

  return (
    <div className="draggle-list-container">
      <div className="draggle-theme">奖品列表——请拖拽产品至奖池</div>
      {isLoading ? (
        <Loading isLoading={isLoading}/>
      ) : (
        <div className="draggle-list">
          {prizeList.map((item) => (
            <PrizeBox key={item._id} prizeInfo={item} />
          ))}
        </div>
      )}
    </div>
  );
}
