import React from "react";
import GridBox from "./components/GridBox";
import DraggleList from "./components/DraggleList";
import { GridProvider } from "context/useGridContext";
import "./index.less";
// 奖池管理
export default function Jackpot() {
  return (
    <GridProvider>
      <div className="add-jackpot-container">
        <GridBox />
        <DraggleList />
      </div>
    </GridProvider>
  );
}
