import React from "react";
import { Suspense } from "react";
import {
  Switch,
  Route,
  Redirect,
  NavLink,
  useLocation,
} from "react-router-dom";
import Loading from "../../components/Loading";
import backendRoute from "../../router/backend.js";
import "./index.less";
import logo from "@/logo.svg";

const navList = [
  { key: "jackpot", navName: "奖池管理", pathname: "/backend/jackpot" },
  { key: "addJackpot", navName: "添加奖池", pathname: "/backend/addjackpot" },
  { key: "prize", navName: "奖品管理", pathname: "/backend/prize" },
];

export default function BackEnd() {
  const location = useLocation();
  const { state: { title = "奖池管理" } = {} } = location;
  return (
    <section className="backend-layout">
      <header className="backend-header">
        <img src={logo} alt="logo" className="logo" />
        {/* 导航栏 */}
        <nav className="backend-navbar">
          {navList.map((item) => {
            return (
              <div className="backend-nav-item" key={item.key}>
                <NavLink
                  to={{
                    pathname: item.pathname,
                    state: { title: item.navName },
                  }}
                  activeClassName="backend-nav-active"
                >
                  {item.navName}
                </NavLink>
              </div>
            );
          })}
        </nav>
      </header>
      <main style={{ padding: "0 50px" }}>
        <div className="backend-breadcrumb">
          <span>后台管理</span>
          <span>&nbsp;\&nbsp;</span>
          <span>{title}</span>
        </div>
        <div className="site-layout-content">
          <Suspense fallback={<Loading/>}>
            <Switch>
              <Redirect exact from="/backend" to="/backend/jackpot" />
              {backendRoute.map((item) => {
                return <Route {...item} />;
              })}
            </Switch>
          </Suspense>
        </div>
      </main>
      <footer className="backend-footer">快乐搞项目小组——抽奖转盘后台</footer>
    </section>
  );
}
