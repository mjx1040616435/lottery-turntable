import React, { useEffect, useState } from "react";
import JackpotBox from "./components/JackpotBox";
import EmptyData from "components/EmptyData";
import Loading from "components/Loading";
import { getViewRoulettes } from "api/backend";
import "./index.less";

export default function JackPot() {
  const [jackpotList, setJackpotList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  // 获取所有奖盘
  useEffect(() => {
    const getData = async () => {
      const data = await getViewRoulettes();
      setJackpotList(data);
      setIsLoading(false);
    };
    getData();
  }, []);
  return isLoading ? (
    <Loading />
  ) : jackpotList.length ? (
    <ul className="jackpot-container">
      {jackpotList.map((item) => {
        return <JackpotBox key={item._id} rouletteInfo={item} potLength={jackpotList.length} />;
      })}
    </ul>
  ) : (
    <EmptyData />
  );
}
