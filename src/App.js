import { Suspense } from "react";
import { Switch,Route,Redirect } from "react-router-dom";
import Loading from "./components/Loading";
import route from "./router";
import './App.less';

function App() {
  return (
    <div className="App">
      <Suspense fallback={<Loading />}>
        <Switch>
          <Redirect exact from="/" to="/turntable"/>
          {
            route.map(item=>{
              return <Route {...item} />
            })
          }
        </Switch>
      </Suspense>
    </div>
  );
}

export default App;
