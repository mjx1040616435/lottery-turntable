import React from "react";
import "./index.less";
export default function Popover({
  appear,
  title,
  tip,
  ensureHandle,
  isCancel,
  cancelHandle,
}) {
  return (
    appear && (
      <div className="pop-box-container">
        <div className="pop_box">
          <div className="pop_header">友情提示</div>
          <div className="pop_content">
            <h1 className="pop_title">{title}</h1>
            {tip && <p className="pop_desc">{tip}一个，请联系管理员！！</p>}
            <div>
              <button className="pop_button" onClick={ensureHandle}>
                确定
              </button>
              {isCancel && (
                <button
                  className="pop_button btn-delete"
                  onClick={cancelHandle}
                >
                  取消
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    )
  );
}
