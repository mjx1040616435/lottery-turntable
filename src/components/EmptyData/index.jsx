import React from 'react';
import "./index.less";

export default function EmptyData() {
  return (
    <div className="empty-data-container">
      <img src="https://lf3-cdn-tos.bytescm.com/obj/static/xitu_juejin_web/img/inprocess.d75949b.png" alt="emptyData" />
      暂无抽奖转盘，等待创建...
    </div>
  )
}
