import React from "react";
import "./index.less";

// 商品列表
export default function PrizeBox({ prizeInfo }) {
  // 被拖动的元素开始拖放的时候触发
  const onDragStart = (e) => {
    const target = e.target;
    const imgPath = target.querySelector("img").src;
    const title = target.innerText.trim();
    const obj = { imgPath, title, id: target.getAttribute("data-id") };
    e.dataTransfer.setData("info", JSON.stringify(obj));
  };
  return (
    <div
      data-id={prizeInfo.prize_id}
      className="prize-box"
      draggable
      onDragStart={onDragStart}
    >
      <img draggable={false} src={prizeInfo.picture_url} alt="prize" />
      <span className="title">{prizeInfo.prize_name}</span>
    </div>
  );
}
