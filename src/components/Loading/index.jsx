import React from "react";
import "./index.less";

export default function Loading({ isLoading=true }) {
  return (
    isLoading && (
      <div className="load-container">
        <i className="spin-item"></i>
        <i className="spin-item"></i>
        <i className="spin-item"></i>
        <i className="spin-item"></i>
      </div>
    )
  );
}
