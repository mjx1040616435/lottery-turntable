/**
 * 后台路由
 */
import { lazy } from "react";
const route = [
  {
    key:'Jackpot',
    path:'/backend/jackpot',
    component:lazy(()=>import('../features/Jackpot')),
    exact:true,
  },
  {
    key:'AddJackpot',
    path:'/backend/addjackpot',
    component:lazy(()=>import('../features/AddJackpot')),
    exact:true,
  },
  {
    key:'Prize',
    path:'/backend/prize',
    component:lazy(()=>import('../features/Prize')),
    exact:true,
  },
]
export default route;