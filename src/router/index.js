/**
 * 首页路由
 */
import { lazy } from "react";
const route = [
  {
    key:'Turntable',
    path:'/turntable',
    component:lazy(()=>import('../features/Turntable')),
    exact:true,
  },
  {
    key:'Backend',
    path:'/backend',
    component:lazy(()=>import('../features/Backend'))
  }
]
export default route;